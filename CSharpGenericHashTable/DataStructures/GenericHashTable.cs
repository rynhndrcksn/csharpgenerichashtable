﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using CSharpGenericHashTable.Interfaces;

/// <summary>
/// Developed by Ryan H.
/// </summary>
namespace CSharpGenericHashTable.DataStructures
{
    /// <summary>
    /// Version of a HashTable that supports generics while using separate chaining to handle collisions.
    /// </summary>
    /// <typeparam name="T">Generic data type.</typeparam>
    public class GenericHashTable<T> : IGenericHashTable<T>
    {
        // Constants
        private const double LoadFactor = 2.5;
        private const int DefaultTableSize = 10;
        private const double TableResizeValue = 1.5;

        // Fields
        private static Node<T>[] table;
        private int size;
        private static int modCount { get; set; }

        /// <summary>
        /// Default constructor that creates a HashTable with the default table size.
        /// </summary>
        public GenericHashTable()
        {
            table = new Node<T>[DefaultTableSize];
        }

        /// <summary>
        /// Parameterized constructor that creates a HashTable of size provided.
        /// </summary>
        /// <param name="size">How many elements the HashTable can support.</param>
        public GenericHashTable(int size)
        {
            table = new Node<T>[size];
        }

        public void Add(T[] elements)
        {
            foreach (T element in elements)
            {
                Add(element);
            }
        }

        private void Add(T element)
        {
            NullCheck(element);

            // Do we contain this element already?
            if (Contains(element))
            {
                throw new ArgumentException($"Element: {element} already exist in the HashTable.");
            }

            // Resize the table if it exceeds the LoadFactor threshold we set.
            double loadFactor = size / table.Length;
            if (loadFactor >= LoadFactor)
            {
                Resize();
            }


            // Get index to place element.
            int index = GetIndex(element);

            // If table[index] is null, our job is easy.
            if (table[index] == null)
            {
                table[index] = new Node<T>(element);
            }
            else
            {
                // Create a temp Node<T> to traverse the chain.
                Node<T> temp = table[index];
                while (temp.next != null)
                {
                    // Need to check if element is in the HashTable.
                    if (temp.data.Equals(element))
                    {
                        return;
                    }
                    // Keep traversing the chain...
                    temp = temp.next;
                }
                // Now that we're at the end of the chain, we can add the element.
                temp.next = new Node<T>(element);
            }
            size++;
            modCount++;
        }

        public void Clear()
        {
            // Purge everything and let the garbage collector take care of it.
            table = new Node<T>[DefaultTableSize];
            size = 0;
            modCount = 0;
        }

        public bool Contains(T element)
        {
            NullCheck(element);

            int index = GetIndex(element);

            // If table[index] isn't null, we traverse the chain trying to find the element.
            if (table[index] != null)
            {
                Node<T> temp = table[index];
                while (temp != null)
                {
                    if (temp.data.Equals(element))
                    {
                        return true;
                    }
                    else
                    {
                        temp = temp.next;
                    }
                }
            }
            return false;
        }

        public T Get(T element)
        {
            NullCheck(element);

            if (Contains(element))
            {
                int index = GetIndex(element);
                Node<T> temp = table[index];
                while (!temp.data.Equals(element))
                {
                    temp = temp.next;
                }
                return temp.data;
            }
            return default;
        }

        public bool IsEmpty()
        {
            return size == 0;
        }

        public void Remove(T element)
        {
            NullCheck(element);

            if (!Contains(element))
            {
                throw new ArgumentException($"Element: ${element} doesn't exist in the hashtable.");
            }

            int index = GetIndex(element);

            // If element is at table[index]
            if (table[index].data.Equals(element))
            {
                // If this index only has 1 element:
                if (table[index].next == null)
                {
                    table[index] = null;
                }
                else // table[index] has a chain:
                {
                    table[index] = table[index].next;
                }
            }
            else // If element isn't at table[index]
            {
                Node<T> current = table[index];
                Node<T> next = current.next;

                while (current.next != null)
                {
                    if (next.data.Equals(element))
                    {
                        // If element is the last Node in a chain
                        if (next.next == null)
                        {
                            current.next = null;
                        }
                        else // If element is in the middle of the chain
                        {
                            current = next.next;
                        }
                    }
                    else
                    {
                        current = next;
                        next = next.next;
                    }
                }
            }
            size--;
            modCount++;
        }

        public int Size()
        {
            return size;
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return (IEnumerator<T>) GetEnumerator();
        }

        public IEnumerator GetEnumerator()
        {
            return new HashTableEnumerator();
        }

        private class HashTableEnumerator : IEnumerator<T>
        {
            private Node<T>[] eTable;
            Node<T> node;
            int index;

            public HashTableEnumerator()
            {
                eTable = table;
                node = eTable[index];
                index = -1;

            }

            public T Current => node.data;

            object IEnumerator.Current => Current;

            public void Dispose()
            {
                eTable = null;
                index = 0;
            }

            public bool MoveNext()
            {
                // This ensures we aren't skipping certain Nodes or doing them twice.
                if ((index < table.Length-1 && node.next == null )|| index == -1)
                {
                    index++;
                    // If we end up on a null, we need to continue advancing the index.
                    while (eTable[index] == null)
                    {
                        index++;
                    }
                    node = table[index];
                    return true;
                }
                // This will push us down the Node chains so they aren't missed.
                else if (node.next != null)
                {
                    node = node.next;
                    return true;
                }
                {
                    return false;
                }
            }

            public void Reset()
            {
                index = -1;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new();
            foreach (var item in table)
            {
                if (item != null)
                {
                    sb.Append($"{item}\n");
                }
                else
                {
                    sb.Append("null\n");
                }
            }
            return sb.ToString();
        }

        // ======== Helper methods are below this line ========

        /// <summary>
        /// Private helper method that returns the index of where the element should be placed.
        /// </summary>
        /// <param name="element">Element to find index of.</param>
        /// <returns>Index of the given element.</returns>
        private int GetIndex(T element)
        {
            int hashCode = Math.Abs(element.GetHashCode());
            return hashCode % table.Length;
        }

        /// <summary>
        /// Private helper method that checks the provided element and throws an ArgumentException if the element is null.
        /// </summary>
        /// <param name="element">Element to check null status of.</param>
        private void NullCheck(T element)
        {
            if (element.Equals(null))
            {
                throw new ArgumentException("Provided element must not be null :/");
            }
        }

        /// <summary>
        /// Private helper method that resizes the HashTable and re-hashes/adds everything.
        /// </summary>
        private void Resize()
        {
            // Save the old table.
            Node<T>[] oldTable = table;
            size = 0;
            modCount = 0;

            // Make a new table that is TableResizeValue larger.
            table = new Node<T>[(int)(table.Length * TableResizeValue)];

            // Move everything over, but rehash it all to keep the chain length down.
            foreach (Node<T> oldElement in oldTable)
            {
                // Adds oldTable[index] element.data
                if (oldElement != null)
                {
                    Add(oldElement.data);
                }
                // Add any extra elements that are chained on.
                if (oldElement.next != null)
                {
                    Node<T> temp = oldElement;
                    // We need to look ahead or else we can run into a NullPointerException.
                    while (temp.next != null)
                    {
                        Add(temp.next.data);
                        temp = temp.next;
                    }
                }
            }
        }
    }
}
