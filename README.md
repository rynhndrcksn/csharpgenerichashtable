# C# Hash Table with generic support

A generic hash table with support for: Add, Clear, Contains, Get, IsEmpty, Remove, Size, and is Enumerable 😊

## Description

This was originally an assignment to port a Java console application to C#. I thought a generic hash table would be a good idea, as it would get me comfortable with C# quickly, and would introduce the more difficult parts of the language (like generic support and enumerable/enumerators).

The hash table only supports adding elements via an array. This was a decision I heavily debated while developing the class. I ultimately went this route, as it easily allows you to add many elements at once, instead of calling .Add() for each element. The downside to this is when you want to add a single element, you need to declare a new array to add it. See [usage](#usage) below for examples.

## Installation

Download the file and import it into any project you need it for.

## Usage

```csharp
using SolutionName.DirectoryName;

static void main(string[] args)
{
    GenericHashTable<int> table = new GenericHashTable<int>();
    // In order to add 1 item, we need to declare a new int array, which is unfortunate.
    table.Add(new int[] {1});
    // But this also allows us to add a lot of numbers at once.
    table.Add(new int[] {2, ..., 1000});

    table.Contains(5); // True
    int five = table.Get(5); // Get returns the object

    table.IsEmpty(); // False
    table.Remove(1);

    table.Size(); // 999
    
    foreach (var item in table)
    {
        Console.WriteLine(item);
    }

    table.Clear();
}

```

## Author
- Ryan Hendrickson - [GitLab](https://gitlab.com/rynhndrcksn), [GitHub](https://github.com/rynhndrcksn)

## Contributing

Contributing file can be found [here](CONTRIBUTING.md).

## License

This project uses the [MIT](LICENSE) license.
