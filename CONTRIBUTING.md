# Contributing

If there is a specific method you would like to see added, or think there are improvements to be made, feel free to open an issue so I can address it.
